IMFR
===

1. 陌生人的處理=>一直瘋狂retry
2. time.sleep的機制=>sleep by fixed timestamp
3. FD/TK library=>opencv haar/eye+dlib
4. FD/FR處理速度控制、data loading control=>根據處理速度，一筆接一筆
5. Face++/QR/FD/TK/FR=>performance analysis for each part
6. image size configuration=>800x450(50x50) dynamic resize
7. test plan、program=>track indication in windowns, multi-thread stress test

Q: thread analysis=>一路camera會有幾條thread?
Q: python opencv讀取rtsp frame會不會有latency?

cpu busy command
seq 3 | xargs -P0 -n1 timeout 5 yes > /dev/null
seq 3 | xargs -P0 -n1 md5sum /dev/zero
3 是 thread number
seq 3 | xargs -P0 -n1 timeout 5 md5sum /dev/zero
timeout 是跑的時間



判斷是no face之後，要不要刪track
直接刪/過十秒刪/always不刪
always不刪=> tk也不能因為它一直做


1. 所有face都塞到同一個queue
2. 統一由一個thread去queue裡抓資料出來，然後用非同步的api丟出request (非同步的worker(thread)數量限制為10)
a. 在送辨識前，檢查該face還在不在track_list中
b. 刪除track時，如果有已經送辨識，但是還沒有處理的face，刪除之 (因為worker只有10，實際上送辨識的只會有10筆，其餘的會先被queue住，刪除那些queue住的)
4. api response回來後會自動執行callback function
5. 從face_list抓出資料送辨識後，固定睡100ms (待送的face很多，每個face送辨識一定會間隔100ms，沒有face就會白睡100ms，然後被wait卡住)

rtsp://172.18.71.11:8554/CH001.sdp

[Python threads synchronization: Locks, RLocks, Semaphores, Conditions, Events and Queues \| Laurent Luce's Blog](https://www.laurentluce.com/posts/python-threads-synchronization-locks-rlocks-semaphores-conditions-events-and-queues/)

[Python线程同步机制: Locks, RLocks, Semaphores, Conditions, Events和Queues - Zhou's Blog](http://yoyzhou.github.io/blog/2013/02/28/python-threads-synchronization-locks/)


curl -F "image=@/home/user/test.png" -F "group=http://127.0.0.1:8866/sync/features" -F "check_quality=False" -F "check_fmp=False" -F "with_landmark=False" -X POST http://127.0.0.1:8080/recognize




curl -F "image=@/home/user/test.png" -F "group=http://127.0.0.1:8866/sync/features?screen_token=LAB_B" -F "check_quality=False" -F "check_fmp=False" -F "with_landmark=False" -X POST http://127.0.0.1:8080/recognize