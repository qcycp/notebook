* 2.1 圖片路徑  
/data/koala_online/upload

* 2.9 圖片路徑  
/koala-data/upload

* updater log  
tail -F /var/log/supervisor/updater-stdout.log | grep POST

* trident  
/usr/local/etc/trident

* 開啟debug模式-（顯示底庫質量分數、識別抓圖質量、置信度）  
chrome瀏覽器，打開開發者控制台，在console中輸入：  
localStorage["debug_score"] = true