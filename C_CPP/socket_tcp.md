socket - TCP
===
[Sockets Programming Example in C: Server Converts Strings to Uppercase \| Programming Logic](https://www.programminglogic.com/sockets-programming-example-in-c-server-converts-strings-to-uppercase/)  
[socket 範例程式--client / server communication @ 心的距離 :: 痞客邦 ::](http://kezeodsnx.pixnet.net/blog/post/27550704-socket-%E7%AF%84%E4%BE%8B%E7%A8%8B%E5%BC%8F--client---server-communication)

gcc -o server server.c  
gcc -o client client.c  
./server  
./client  

```c
//server.c
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>

int main(){
  int welcomeSocket, newSocket, portNum, clientLen, nBytes;
  char buffer[1024];
  struct sockaddr_in serverAddr;
  struct sockaddr_storage serverStorage;
  socklen_t addr_size;
  int i;

  welcomeSocket = socket(PF_INET, SOCK_STREAM, 0);

  portNum = 7891;

  serverAddr.sin_family = AF_INET;
  serverAddr.sin_port = htons(portNum);
  serverAddr.sin_addr.s_addr = inet_addr("0.0.0.0");
  memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);

  bind(welcomeSocket, (struct sockaddr *) &serverAddr, sizeof(serverAddr));

  if(listen(welcomeSocket,5)==0)
    printf("Listening\n");
  else
    printf("Error\n");

  addr_size = sizeof serverStorage;

  /*loop to keep accepting new connections*/
  while(1){
    newSocket = accept(welcomeSocket, (struct sockaddr *) &serverStorage, &addr_size);
    printf("New client(%d) connecting...\n", newSocket);
    /*fork a child process to handle the new connection*/
    if(!fork()){
      nBytes = 1;
      /*loop while connection is live*/
      while(nBytes!=0){
        memset(buffer, 0, sizeof(buffer));
        nBytes = recv(newSocket,buffer,1024,0);
        printf("Received from client(%d): %s\n\n",newSocket, buffer);
        memset(buffer, 0, sizeof(buffer));
        //send(newSocket,buffer,nBytes,0);
      }
      printf("Client(%d) closing...\n", newSocket);
      close(newSocket);
      exit(0);
    }
    /*if parent, close the socket and go back to listening new requests*/
    else{
      printf("Client(%d) closing...\n", newSocket);
      close(newSocket);
    }
  }

  return 0;
}
```

```c
//client.c
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>

int main(){
  int clientSocket, portNum, nBytes;
  char buffer[1024];
  struct sockaddr_in serverAddr;
  socklen_t addr_size;

  clientSocket = socket(PF_INET, SOCK_STREAM, 0);

  portNum = 7891;

  serverAddr.sin_family = AF_INET;
  serverAddr.sin_port = htons(portNum);
  serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
  memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);

  addr_size = sizeof serverAddr;
  connect(clientSocket, (struct sockaddr *) &serverAddr, addr_size);

  while(1){
    printf("Type a sentence to send to server:\n");
    fgets(buffer,1024,stdin);
    printf("You typed: %s",buffer);

    nBytes = strlen(buffer) + 1;

    send(clientSocket,buffer,nBytes,0);

    recv(clientSocket, buffer, 1024, 0);

    printf("Received from server: %s\n\n",buffer);
  }

  return 0;
}
```