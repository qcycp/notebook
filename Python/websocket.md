python websocket
===
```bash
# requirements.txt
six==1.11.0
websocket-client==0.53.0
websocket-server==0.4
```

```python
# client.py
from urllib.parse import urlencode
import websocket
import threading
import time
import sys
import json

port = 8866

def on_message(ws, message):
    print("receive message from server: " + message)

def on_error(ws, error):
    print(error)

def on_close(ws):
    print("### closed ###")

def on_open(ws):
    def job():
        while True:
            time.sleep(5)
            ws.send("Hello")
        time.sleep(1)
        ws.close()
    thread = threading.Thread(target=job)
    thread.start()


if __name__ == "__main__":
    websocket.enableTrace(True)
    #ws = websocket.create_connection("ws://localhost:" + str(port) + "/")
    url = {'url': 'rtsp://10.101.1.60/live1.sdp'}
    ws = websocket.WebSocketApp("ws://127.0.0.1:" + str(port) + "/ws?" + urlencode(url),
                                on_message = on_message,
                                on_error = on_error,
                                on_close = on_close)
    ws.on_open = on_open
    ws.run_forever()
```
```bash
pip install git+https://github.com/Pithikos/python-websocket-server (latest code)
pip install websocket-server (might not be up-to-date)
```

```python
# server.py
from websocket_server import WebsocketServer
import time

clients = []

# Called for every client connecting (after handshake)
def new_client(client, server):
    clients.append(client)
    print("New client connected and was given id %d" % client['id'])
#    while True:
    server.send_message_to_all("New client connected and was given id %d" % client['id'])
#        time.sleep(3)


# Called for every client disconnecting
def client_left(client, server):
        print("Client(%d) disconnected" % client['id'])


# Called when a client sends a message
def message_received(client, server, message):
    clients.remove(client)
    if len(message) > 200:
        message = message[:200]+'..'
    print("Client(%d) said: %s" % (client['id'], message))


PORT=9001
server = WebsocketServer(PORT)
server.set_fn_new_client(new_client)
server.set_fn_client_left(client_left)
server.set_fn_message_received(message_received)
server.run_forever()
```