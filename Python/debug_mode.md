```python
if __debug__:
    print "In debug mode"
else:
    print "Not in debug mode"
```

python filename.py  
=> 跑 In debug mode  
python -O filename.py  
=> 跑 Not in debug mode