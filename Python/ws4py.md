websocket with ws4py
===
```bash
#requirements.txt
gevent==1.3.7
greenlet==0.4.15
ws4py==0.5.1
```
```python
# WSGIServer.pylinux常用指令
from ws4py.server.geventserver import WSGIServer
from ws4py.server.wsgiutils import WebSocketWSGIApplication
from ws4py.websocket import WebSocket
import time
import gevent
import json

class ClientManager(object):
    clients = []

    @classmethod
    def add_client(cls, client):
        cls.clients.append(client)

    @classmethod
    def send_msg_to_all_client(cls, m):
        while True:
            gevent.sleep(5)
 
            m['data']['timestamp'] = int(time.time())
            m['data']['status'] = 'recognizing'
            m['data']['recognized'] = True
            for client in cls.clients:
                client.send(json.dumps(m))

            gevent.sleep(10)

            m['data']['timestamp'] = int(time.time())
            m['data']['status'] = 'gone'
            for client in cls.clients:
                client.send(json.dumps(m))

            gevent.sleep(5)

            m['data']['timestamp'] = int(time.time())
            m['data']['status'] = 'recognizing'
            m['data']['recognized'] = False
            for client in cls.clients:
                client.send(json.dumps(m))


    @classmethod
    def close_client(cls, client):
        if client in cls.clients:
            cls.clients.remove(client)

class WebsocketHandler(WebSocket):
    def opened(self):
        print("New client connected");
        ClientManager.add_client(self)

    def received_message(self, m):
        print("receive message from client: " + str(m))

    def closed(self, code, reason):
        ClientManager.close_client(self)

message = {
    'type': 'track',
    'data': {
        'track': 121212,
        'recognized': True,
        'status': 'recognizing',
        'timestame': int(time.time()),
        'person': {
            'subject_id': 200,
            'confidence': 97.123,
            'feature_id': 121321
        },
        'camera': {
            'camera_id': 7
        }
    }
}

gevent.spawn(ClientManager.send_msg_to_all_client, message)

server = WSGIServer(('0.0.0.0', 9010), WebSocketWSGIApplication(handler_cls=WebsocketHandler))
server.serve_forever()
```