flask hot reload
===
在debug模式下，有任何修改都會自動reload server  
```shell
(venv) $ set FLASK_APP=main.py
(venv) $ set FLASK_DEBUG=1
(venv) $ flask run

 * Detected change in 'C:\\tmp\\work\\20180828_kangaroo\\backend\\kangaroo\\app\\views\\subject.py', reloading
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 296-270-139
 * Running on http:\\127.0.0.1:5000\ (Press CTRL+C to quit)
```