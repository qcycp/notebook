.vimrc
===
```bash
set number
set hlsearch
set autoindent
set smartindent
set ic
set nowrap
hi Comment      term=bold               ctermfg=DarkCyan
set tabstop=4
set shiftwidth=4
set laststatus=2
set statusline=%4*%<\%m%<[%f\%r%h%w]%=%l,%v\ \ \ \ \ \ \ \ %p%%
```