查詢一個process在執行期間，context switch的次數

* voluntary_ctxt_switches: 自願  
* nonvoluntary_ctxt_switches: 強制  

```sh
user@vm-docker:~$ grep ctxt /proc/6415/status
voluntary_ctxt_switches:        80757
nonvoluntary_ctxt_switches:     805
```

* pidstat -w 1: 查看process切换的每秒統計值

```sh
user@vm-docker:~$ sudo apt-get install sysstat
user@vm-docker:~$ pidstat -w 1
Linux 4.4.0-131-generic (vm-docker)     04/02/2019      _x86_64_        (4 CPU)

05:14:06 PM   UID       PID   cswch/s nvcswch/s  Command
05:14:07 PM     0         7     38.61      0.00  rcu_sched
05:14:07 PM     0        23     19.80      0.00  ksoftirqd/3
05:14:07 PM     0        29      0.99      0.00  khungtaskd
```