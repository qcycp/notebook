RadioButton OnClickListener
===

```java
final RadioGroup radioGroup = layout.findViewById(R.id.radio_group);
final RadioButton channel1 = radioGroup.findViewById(R.id.channel1);
channel1.setOnClickListener(new View.OnClickListener() {
    public void onClick(View v) {
    }
});
```