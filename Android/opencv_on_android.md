opencv for android
===
[Building OpenCV & OpenCV Extra Modules For Android From Source \| You, Myself and Community](https://zami0xzami.wordpress.com/2016/03/17/building-opencv-for-android-from-source/)

1. download opencv  
[GitHub - opencv/opencv: Open Source Computer Vision Library](https://github.com/opencv/opencv)
git clone https://github.com/opencv/opencv.git

2. download opencv extra module  
[GitHub - opencv/opencv_contrib: Repository for OpenCV's extra modules](https://github.com/opencv/opencv_contrib)
