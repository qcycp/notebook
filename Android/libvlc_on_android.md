libvlc on Android for playing rtsp streaming
===
https://github.com/SteinerOk/libvlc-sdk-android  
https://github.com/mrmaffen/vlc-android-sdk  

以下兩個都可正常使用  
```xml
dependencies {
    //implementation 'com.steiner.videolan:libvlc-android:3.0.0'
    compile 'de.mrmaffen:libvlc-android:2.1.12@aar'
}
```