Android 常用系統事件
===

| 事件常量 | 描述 |
|:--|:--|
|android.intent.action.BATTERY_CHANGED|持久的廣播，包含電池的充電狀態，級別和其他信息。|
|android.intent.action.BATTERY_LOW|標識設備的低電量條件。|
|android.intent.action.BATTERY_OKAY|標識電池在電量低之後，現在已經好了。|
|android.intent.action.BOOT_COMPLETED|在系統完成啓動後廣播一次。|
|android.intent.action.BUG_REPORT|顯示報告bug的活動。|
|android.intent.action.CALL|執行呼叫數據指定的某人。|
|android.intent.action.CALL_BUTTON|用戶點擊"呼叫"按鈕打開撥號器或者其他撥號的合適界面。|
|android.intent.action.DATE_CHANGED|日期發生改變。|
|android.intent.action.REBOOT|設備重啓。|