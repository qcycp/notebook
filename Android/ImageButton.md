ImageButton
===
```xml
<ImageButton
    android:id="@+id/imageButton"
    android:layout_width="50dp"
    android:layout_height="50dp"
    android:scaleType="fitCenter"
    android:background="@android:color/transparent"
    android:src="@drawable/settings"
    android:alpha=".5" />
```