webview開發
===

[Android WebView Memory Leak WebView内存泄漏-腾讯云资讯](https://cloud.tencent.com/info/1c147afe5f05995d8eefb9e80e8705bf.html)

[【Android】 WebView内存泄漏优化之路 \| KaKa's blog](https://lipeng1667.github.io/2016/08/06/memory-optimisation-for-webview-in-android/)

[android内存优化之webview - 简书](https://www.jianshu.com/p/c2412918b2b5)


log system in Android
===

[GitHub - elvishew/xLog: Android logger, simple and pretty, powerful and flexible, all you need is here (updating)](https://github.com/elvishew/xLog)


Android TCP socket
===

[Android Socket範例 @ SIN-Android學習筆記 :: 痞客邦 ::](http://xxs4129.pixnet.net/blog/post/164402593-android-socket%E7%AF%84%E4%BE%8B)

[《Android》『Socket』- 如何透過 Socket 連線連接用戶端與伺服端程式 - 賽肥膩膩の食旅生活誌](https://xnfood.com.tw/socket/)

[HsingJung Chen’s 筆記本: Android：使用Socket的簡單聊天室範例](http://hsingjungchen.blogspot.com/2017/07/androidsocket.html)
