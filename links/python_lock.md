python lock
===

[Python threads synchronization: Locks, RLocks, Semaphores, Conditions, Events and Queues \| Laurent Luce's Blog](https://www.laurentluce.com/posts/python-threads-synchronization-locks-rlocks-semaphores-conditions-events-and-queues/)

[Python线程同步机制: Locks, RLocks, Semaphores, Conditions, Events和Queues - Zhou's Blog](http://yoyzhou.github.io/blog/2013/02/28/python-threads-synchronization-locks/)

[Python Multithreading Tutorial: Condition objects with Producer and consumer - 2019](https://www.bogotobogo.com/python/Multithread/python_multithreading_Synchronization_Condition_Objects_Producer_Consumer.php)