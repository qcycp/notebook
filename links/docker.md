docker-squash
===

[GitHub - goldmann/docker-squash: Docker image squashing tool](https://github.com/goldmann/docker-squash)

[深入解析 Docker 镜像机制 -- 为何 Docker 镜像 ID 会显示 missing ?『译』 \| Valyria](https://darcysail.com/2019/01/09/Inside-Docker-images-and-layers/)


dlib install in docker image
===

[face_recognition/Dockerfile at master · ageitgey/face_recognition · GitHub](https://github.com/ageitgey/face_recognition/blob/master/Dockerfile)

[alpine-dlib/Dockerfile at master · c9s/alpine-dlib · GitHub](https://github.com/c9s/alpine-dlib/blob/master/edge/Dockerfile)

[dlib安装的那些破事 - 简书](https://www.jianshu.com/p/772ccb948f1a)

[docker-python-opencv3-dlib/Dockerfile at master · lapidarioz/docker-python-opencv3-dlib · GitHub](https://github.com/lapidarioz/docker-python-opencv3-dlib/blob/master/Dockerfile)


Flask
===

[[ Docker ] 建立 Docker 版的 Flask CRUD](https://oranwind.org/docker/)


elasticseatch in docker
===

[docker-flask-elasticsearch-example/docker-compose.yml at master · zouzias/docker-flask-elasticsearch-example · GitHub](https://github.com/zouzias/docker-flask-elasticsearch-example/blob/master/docker-compose.yml)


docker useful link
===
[使用docker搭建nginx，flask，gunicorn运行环境 - 简书](https://www.jianshu.com/p/f5c271d95e39)

[用docker部署flask+gunicorn+nginx - 永远的幻想 - 博客园](https://www.cnblogs.com/xuanmanstein/p/7692256.html)