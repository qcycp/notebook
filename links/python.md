python + opencv
===

[【Python+OpenCV入門學習】四、視頻的讀取、顯示、保存 - 神馬文庫](https://www.smwenku.com/a/5ba303612b71771a4daa103a/)

[opencv python 从摄像头获取视频/从文件获取视频 /保存视频 - 个人文章 - SegmentFault 思否](https://segmentfault.com/a/1190000015575701)

[读取多个(海康)网络摄像头的实时视频流 (使用opencv-python)，解决实时读取延迟问题 - 知乎](https://zhuanlan.zhihu.com/p/38136322)

[Python/DEMO_camera_rtsp_cv2.VideoCapture.py at master · Yonv1943/Python · GitHub](https://github.com/Yonv1943/Python/blob/master/Demo/DEMO_camera_rtsp_cv2.VideoCapture.py)


python-crontab
===

[python-crontab · PyPI](https://pypi.org/project/python-crontab/)

[Scheduling Jobs with python-crontab](https://stackabuse.com/scheduling-jobs-with-python-crontab/)


uwsgi and flask
===

[优雅重载的艺术 — uWSGI 2.0 文档](https://uwsgi-docs-zh.readthedocs.io/zh_CN/latest/articles/TheArtOfGracefulReloading.html)

python tornado websocket
===

[Maxkit: python tornado websocket server and client](http://blog.maxkit.com.tw/2017/08/python-tornado-websocket-server-and.html)

