##### GET
curl http://192.168.56.102:8857/api/subject/list  
##### POST
* curl -d "username=admin&password=123456" http://192.168.56.102:8857/api/auth/login
* curl -H "Content-Type:application/json" -X POST -d "{'username': 'admin', 'password': '123456'}" http://192.168.56.102:8857/api/auth/login  
* curl -F "image=@/home/user/test.jpg" -X POST http://10.60.6.29:8866/recognize  
* curl -F "image=@/home/user/test.jpg" -F "screen_token=FaceBox_X96" -X POST http://10.60.6.29:8866/recognize

[使用curl指令測試REST服務](http://blog.kent-chiu.com/2013/08/14/testing-rest-with-curl-command.html)

(-d, --data) and multipart formpost (-F, --form)

curl -X GET "http://www.rest.com/api/users"  
curl -X POST "http://www.rest.com/api/users"  
curl -X PUT "http://www.rest.com/api/users"  
curl -X DELETE "http://www.rest.com/api/users"  


curl -X GET http://192.168.56.3:5074/api/ipcam  
curl -X DELETE id "id=0" http://192.168.56.3:5074/api/ipcam  
curl -X POST -d "Host=192.168.56.1&Location=LAB_C&RelayDelay=0.15&RelayIP=192.168.1.199&RelayPort=12345&Token=NO_2&URL=E6-IN-01.mp4" http://192.168.56.3:5074/api/ipcam  
curl -X PUT -d "id=0&Host=192.168.56.1&Location=LAB_C&RelayDelay=0.15&RelayIP=192.168.1.199&RelayPort=12345&Token=NO_2&URL=E6-IN-01.mp4" http://192.168.56.3:5074/api/ipcam