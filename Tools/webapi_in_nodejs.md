call web api in Nodejs
===
```js
var request = require('request');

app.post('/api', function (req, res) {
    var payload = {"name":req.body.name, "password":req.body.password}
    request.post({url:'http://192.168.21.84:8080/api', form:payload}, function(err, response, body) {
        if (!err && response.statusCode == 200) {
            console.log(body);
            res.send(body);
        }
    });
});

app.get('/api', function (req, res) {
    request('http://192.168.21.84:8080/api?abc=123', function (error, response, body) {
        if (!err && response.statusCode == 200) {
            console.log(body);
            res.send(body);
        }
    });
});
```